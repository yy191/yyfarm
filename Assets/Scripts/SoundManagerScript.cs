﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerScript : MonoBehaviour {
    public static AudioClip bgm, water, cut, coin;
    static AudioSource audioSrc;

	// Use this for initialization
	void Start () {
        water = Resources.Load<AudioClip>("Sound/water");
        cut = Resources.Load<AudioClip>("Sound/cut");
        coin = Resources.Load<AudioClip>("Sound/coin");
        bgm = Resources.Load<AudioClip>("Sound/bgm");

        audioSrc = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public static void PlaySound(string clip)
    {
        switch (clip)
        {
            case "water":
                audioSrc.PlayOneShot(water);
                break;
            case "cut":
                audioSrc.PlayOneShot(cut);
                break;
            case "coin":
                audioSrc.PlayOneShot(coin);
                break;

        }
    }
}
