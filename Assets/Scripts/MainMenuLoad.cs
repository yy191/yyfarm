﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuLoad : MonoBehaviour {
    public void PlayGame()
    {

        SceneManager.LoadScene("Scenes/SampleScene");
    }
    public void Quit()
    {
        Application.Quit();
    }
    public void Help()
    {
        SceneManager.LoadScene("Scenes/Help");
    }
    public void Return()
    {
        SceneManager.LoadScene("Scenes/MainMenu");
    }
}
