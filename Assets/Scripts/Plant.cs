﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plant : MonoBehaviour
{
    public Vector3 pos;
    //private GameObject anim;a
    public Sprite noPlantObj;
    public static string toolSelected = "seed1";
    private string[] toolSet = { "seed1", "seed2", "sickle", "water" };
    public static float[,] spawnGrid = new float[30,2]{{0.788f, -1.628f},
    {0.7890003f, -2.262f},
    {0.1610999f, -1.628f},
    {0.161f, -2.262f},
    {-0.4709001f, -1.628f},
    {-0.471f, -2.262f},
    {-1.0999f, -1.628f},
    {-1.1f, -2.262f},
    {-1.7295f, -1.628f},
    {-1.7296f, -2.262f},
    {-2.361f, -1.628f},
    {-2.3611f, -2.262f},
    {-2.99f, -1.628f},
    {-2.9901f, -2.262f},
    {0.789f, -2.897f},
    {0.1570001f, -2.897f},
    {-0.4750001f, -2.896f},
    {-1.111f, -2.896f},
    {-1.731f, -2.896f},
    {-2.366f, -2.896f},
    {-2.993f, -2.896f},
    {-3.619f, -1.628f},
    {-3.625f, -2.262f},
    {-3.619f, -2.896f},
    {-4.245f, -1.628f},
    {-4.246f, -2.262f},
    {-4.246f, -2.896f},
    {-4.873f, -1.628f},
    {-4.879f, -2.262f},
    {-4.879f, -2.896f}};
    //IF there exist a plant and which kind
    //public static int[,] plants = new int[, ]{};
    public static GameObject seedTool1;
    public static GameObject seedTool2;
    public static GameObject sickleTool;
    public static GameObject waterTool;


    //private float halfDeltaY = 0.2215f;
    //private float halfDeltaX = 0.313455f;

    // Use this for initialization
    void Start () {
        pos = transform.position;
        //Debug.Log(pos);
        seedTool1 = GameObject.Find("farming-tileset_78");
        seedTool2 = GameObject.Find("farming-tileset_78_1");
        sickleTool = GameObject.Find("farming-tileset_104");
        waterTool = GameObject.Find("farming-tileset_109");
        seedTool1.transform.localScale = new Vector3(2, 2, 1);




    }
    // Update is called once per frame
    void Update () {
        //press "K"
        if (Input.GetKeyDown(KeyCode.K))
        {
            switch (toolSelected)
            {
                case "seed1":
                    toolSelected = "seed2";
                    Debug.Log("Holding seed2");
                    seedTool2.transform.localScale = new Vector3(2, 2, 1);
                    seedTool1.transform.localScale = new Vector3(1, 1, 1);
                    sickleTool.transform.localScale = new Vector3(1, 1, 1);
                    waterTool.transform.localScale = new Vector3(1, 1, 1);
                    break;
                case "seed2":
                    toolSelected = "sickle";
                    Debug.Log("Holding sickle");
                    seedTool1.transform.localScale = new Vector3(1, 1, 1);
                    seedTool2.transform.localScale = new Vector3(1, 1, 1);
                    sickleTool.transform.localScale = new Vector3(2, 2, 1);
                    waterTool.transform.localScale = new Vector3(1, 1, 1);
                    break;
                case "sickle":
                    toolSelected = "water";
                    Debug.Log("Holding water");
                    sickleTool.transform.localScale = new Vector3(1, 1, 1);
                    seedTool1.transform.localScale = new Vector3(1, 1, 1);
                    seedTool2.transform.localScale = new Vector3(1, 1, 1);
                    waterTool.transform.localScale = new Vector3(2, 2, 1);
                    break;
                case "water":
                    toolSelected = "seed1";
                    Debug.Log("Holding seed1");
                    waterTool.transform.localScale = new Vector3(1, 1, 1);
                    sickleTool.transform.localScale = new Vector3(1, 1, 1);
                    seedTool2.transform.localScale = new Vector3(1, 1, 1);
                    seedTool1.transform.localScale = new Vector3(2, 2, 1);
                    break;
            }

        }

        //Debug.Log(transform.position.y); //x+4, y-1
        if (transform.position.x > -5.0f && transform.position.x < 1.0f && transform.position.y > -3.0f && transform.position.y < -1.4f)
        {
            Debug.Log("In circle");


            //plant
            //string plantName = "";
            if (Input.GetKeyDown(KeyCode.J))
            {
                switch (toolSelected)
                {
                    case "seed1":
                        Debug.Log("J pressed, seed1 using");
                        Object[] backSprites = Resources.LoadAll("Farming2/farming-tileset");
                        //plantName = 
                        GameObject plant = new GameObject("corn");



                        plant.transform.localScale = new Vector3(2, 2, 1);
                        //Search for closest obj
                        //plant.transform.position = transform.position;
                        plant.transform.position = SearchClosestObj(transform.position); 
                        SpriteRenderer rend = plant.AddComponent(typeof(SpriteRenderer)) as SpriteRenderer;


                        //Assign the sprite to the SpriteRenender
                        rend.sprite = (Sprite)backSprites[10];
                        rend.sortingLayerName = "animals";
                        SoundManagerScript.PlaySound("coin");
                        break;
                    case "seed2":
                        Debug.Log("J pressed, seed1 using");
                        Object[] backSprites1 = Resources.LoadAll("Farming2/farming-tileset");
                        GameObject plant1 = new GameObject("corn");



                        plant1.transform.localScale = new Vector3(2, 2, 1);
                        //Search for closest obj
                        //plant.transform.position = transform.position;
                        plant1.transform.position = SearchClosestObj(transform.position);
                        SpriteRenderer rend1 = plant1.AddComponent(typeof(SpriteRenderer)) as SpriteRenderer;


                        //Assign the sprite to the SpriteRenender
                        rend1.sprite = (Sprite)backSprites1[10];
                        rend1.sortingLayerName = "animals";
                        SoundManagerScript.PlaySound("coin");
                        break;


                    case "sickle": //liandao
                        Debug.Log("J pressed, sickle holding");
                        Object[] backSprites2 = Resources.LoadAll("Farming2/farming-tileset");



                        //Destroy();
                        Vector3 x = SearchClosestObj(transform.position);
                        GameObject plant2 = new GameObject("none");
                        plant2.transform.localScale = new Vector3(2, 2, 1);
                        plant2.transform.position = x;
                        SpriteRenderer rend2 = plant2.AddComponent(typeof(SpriteRenderer)) as SpriteRenderer;
                        ////Assign the sprite to the SpriteRenender
                        rend2.sprite = (Sprite)backSprites2[9];
                        rend2.sortingLayerName = "animals";

                        SoundManagerScript.PlaySound("cut");
                        break;

                    case "water"://TODO
                        Debug.Log("J pressed, water pot holding");

                        SoundManagerScript.PlaySound("water");
                        Vector3 x2 = SearchClosestObj(transform.position);

                        break;
                        



                }


        }
        }


	}

    private Vector3 SearchClosestObj(Vector3 posCharacter)
    {
        float min = 1000f;
        float x = 1000f;
        float y = 1000f;
        int i, min_i = 0;
        for (i = 0; i < 30; ++i)
        {
        float dist = Mathf.Abs(posCharacter.x - spawnGrid[i, 0]) + Mathf.Abs(posCharacter.y - spawnGrid[i, 1]);
            //if (dist.x < halfDeltaX && dist.x > -halfDeltaX && dist.y > -halfDeltaY && dist.y < halfDeltaY)
            //{
            //    break;
            //}
            if (dist < min)
            {
                min = dist;
                x = spawnGrid[i, 0];
                y = spawnGrid[i, 1];
                min_i = i;
            }
        }
        Vector3 posObj = new Vector3(x,y,0);

        if(toolSelected == "water")
            Grow.isWatered[min_i] = 1;
        if(toolSelected == "sickle")
        {
            if (Grow.plants[min_i] == 3)
                ScoreDisplayer.score += 3;
            if (Grow.plants[min_i] == 8)
                ScoreDisplayer.score += 5;
            Grow.plants[min_i] = 0;
        }
        //plant seed1
        if (Grow.plants[min_i] == 0)
        {
            if (toolSelected == "seed1")
                Grow.plants[min_i] = 1;
            //plant seed2
            else if (toolSelected == "seed2")
                Grow.plants[min_i] = 4;
        }
        else 
            posObj = new Vector3(-10,-10,0);

        return posObj;
    }
}
