﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grow : MonoBehaviour {
    public static float day = 0f;
    public static int[] plants = new int[30];
    public static int[] isWatered = new int[30];

    // Use this for initialization
    void Start () {
        //for (int i = 0; i < 30; ++i)
        //    plants[i] = 0;
        //for (int i = 0; i < 30; ++i)
        //isWatered[i] = 0;
        renewImage();


    }
	
	// Update is called once per frame
	void Update () {
		
	}
    public static int[] getPlant()
    {
        return plants;
    }


    public static void updatePlantStage()
    {
        for (int i = 0; i < 30; ++i)
        {
            if (Grow.plants[i] != 0 && Grow.plants[i] != 9 && Grow.plants[i] != 3 && Grow.plants[i] != 8)
            {
                if (Grow.isWatered[i] == 0){
                    Grow.plants[i] = 9;
                }
                   
                else{
                    Grow.plants[i] += 1;
                }
                    

            }


        }
    }
    public static void renewImage()
    {
        day += 1f;
        
        for (int i = 0; i < 30; ++i)
        {
            switch(plants[i])
            {

                case 1:
                    GameObject plant = new GameObject("corn1");
                    plant.transform.localScale = new Vector3(2, 2, 1);
                    plant.transform.position = new Vector3( Plant.spawnGrid[i,0], Plant.spawnGrid[i, 1], 0);
                    SpriteRenderer rend = plant.AddComponent(typeof(SpriteRenderer)) as SpriteRenderer;
                    //Assign the sprite to the SpriteRenender
                    rend.sprite = (Sprite)Resources.LoadAll("Farming2/farming-tileset")[29];
                    rend.sortingLayerName = "animals";
                    break;
                case 2:
                    GameObject plant2 = new GameObject("corn1");
                    plant2.transform.localScale = new Vector3(2, 2, 1);
                    plant2.transform.position = new Vector3(Plant.spawnGrid[i, 0], Plant.spawnGrid[i, 1], 0);
                    SpriteRenderer rend2 = plant2.AddComponent(typeof(SpriteRenderer)) as SpriteRenderer;
                    //Assign the sprite to the SpriteRenender
                    rend2.sprite = (Sprite)Resources.LoadAll("Farming2/farming-tileset")[42];
                    rend2.sortingLayerName = "animals";
                    break;
                case 3:
                    GameObject plant3 = new GameObject("corn1");
                    plant3.transform.localScale = new Vector3(2, 2, 1);
                    plant3.transform.position = new Vector3(Plant.spawnGrid[i, 0], Plant.spawnGrid[i, 1], 0);
                    SpriteRenderer rend3 = plant3.AddComponent(typeof(SpriteRenderer)) as SpriteRenderer;
                    //Assign the sprite to the SpriteRenender
                    rend3.sprite = (Sprite)Resources.LoadAll("Farming2/farming-tileset")[55];
                    rend3.sortingLayerName = "animals";
                    break;
                case 4:
                    GameObject plant4 = new GameObject("corn1");
                    plant4.transform.localScale = new Vector3(2, 2, 1);
                    plant4.transform.position = new Vector3(Plant.spawnGrid[i, 0], Plant.spawnGrid[i, 1], 0);
                    SpriteRenderer rend4 = plant4.AddComponent(typeof(SpriteRenderer)) as SpriteRenderer;
                    //Assign the sprite to the SpriteRenender
                    rend4.sprite = (Sprite)Resources.LoadAll("Farming2/farming-tileset")[30];
                    rend4.sortingLayerName = "animals";
                    break;
                case 5:
                    GameObject plant5 = new GameObject("corn1");
                    plant5.transform.localScale = new Vector3(2, 2, 1);
                    plant5.transform.position = new Vector3(Plant.spawnGrid[i, 0], Plant.spawnGrid[i, 1], 0);
                    SpriteRenderer rend5 = plant5.AddComponent(typeof(SpriteRenderer)) as SpriteRenderer;
                    //Assign the sprite to the SpriteRenender
                    rend5.sprite = (Sprite)Resources.LoadAll("Farming2/farming-tileset")[30];
                    rend5.sortingLayerName = "animals";
                    break;
                case 6:
                    GameObject plant6 = new GameObject("corn1");
                    plant6.transform.localScale = new Vector3(2, 2, 1);
                    plant6.transform.position = new Vector3(Plant.spawnGrid[i, 0], Plant.spawnGrid[i, 1], 0);
                    SpriteRenderer rend6 = plant6.AddComponent(typeof(SpriteRenderer)) as SpriteRenderer;
                    //Assign the sprite to the SpriteRenender
                    rend6.sprite = (Sprite)Resources.LoadAll("Farming2/farming-tileset")[43];
                    rend6.sortingLayerName = "animals";
                    break;
                case 7:
                    GameObject plant7 = new GameObject("corn1");
                    plant7.transform.localScale = new Vector3(2, 2, 1);
                    plant7.transform.position = new Vector3(Plant.spawnGrid[i, 0], Plant.spawnGrid[i, 1], 0);
                    SpriteRenderer rend7 = plant7.AddComponent(typeof(SpriteRenderer)) as SpriteRenderer;
                    //Assign the sprite to the SpriteRenender
                    rend7.sprite = (Sprite)Resources.LoadAll("Farming2/farming-tileset")[43];
                    rend7.sortingLayerName = "animals";
                    break;
                case 8:
                    GameObject plant8 = new GameObject("corn1");
                    plant8.transform.localScale = new Vector3(2, 2, 1);
                    plant8.transform.position = new Vector3(Plant.spawnGrid[i, 0], Plant.spawnGrid[i, 1], 0);
                    SpriteRenderer rend8 = plant8.AddComponent(typeof(SpriteRenderer)) as SpriteRenderer;
                    //Assign the sprite to the SpriteRenender
                    rend8.sprite = (Sprite)Resources.LoadAll("Farming2/farming-tileset")[56];
                    rend8.sortingLayerName = "animals";
                    break;
                case 9:
                    GameObject plant9 = new GameObject("corn1");
                    plant9.transform.localScale = new Vector3(2, 2, 1);
                    plant9.transform.position = new Vector3(Plant.spawnGrid[i, 0], Plant.spawnGrid[i, 1], 0);
                    SpriteRenderer rend9 = plant9.AddComponent(typeof(SpriteRenderer)) as SpriteRenderer;
                    //Assign the sprite to the SpriteRenender
                    rend9.sprite = (Sprite)Resources.LoadAll("Farming2/farming-tileset")[11];
                    rend9.sortingLayerName = "animals";
                    break;
                
            }
        }
    }
}
