﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CowController : MonoBehaviour {

    float time = 0f;
    //int timeRemaining;
    // Use this for initialization
    GameObject plant2 = null;
    void Start () {
        //Grow.renewImage();

    }

    // Update is called once per frame
    void Update () {
        if ((Time.time - time) >= 2.0f && plant2 != null)
        {
            //Debug.Log("in if");
            Destroy(plant2);
        }
    }
    void OnCollisionEnter2D(){
        plant2 = new GameObject("heart");
        time = Time.time;
        //Debug.Log(time);
        Object[] backSprites2 = Resources.LoadAll("Farming/farming-emotions");
        plant2.transform.localScale = new Vector3(2, 2, 1);
        plant2.transform.position = transform.position;
        SpriteRenderer rend2 = plant2.AddComponent(typeof(SpriteRenderer)) as SpriteRenderer;
        //Assign the sprite to the SpriteRenender
        rend2.sprite = (Sprite)backSprites2[5];
        rend2.sortingLayerName = "dialog";



    }
}
